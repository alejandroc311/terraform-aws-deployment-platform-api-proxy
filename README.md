# Redux React Platform API Proxy

NGINX Proxy Server for Redux/React platform available at: `https://github.com/alejandroc311/redux-react-platform`. 

## Usage
Serve image files to client from AWS S3 bucket and load balance to Express API

### Environment Variables
  * `LISTEN_PORT` - Port to listen on (default `8080`)
  * `S3_BUCKET` - URL for Amazon S3 Bucket that contains users' images.
  * `S3_BUCKET_KEY` - Unique key for Amazon S3 Bucket that contains users' images.