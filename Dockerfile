FROM nginxinc/nginx-unprivileged:1-alpine
COPY ./mynginx.conf /etc/nginx/nginx.conf.default
COPY ./entrypoint.sh /entrypoint.sh
ENV LISTEN_PORT=8080
ENV S3_BUCKET=http://aws-terraform-deployment-react-redux-platform-image-bucket-s3.s3.amazonaws.com/
ENV S3_BUCKET_KEY=aws-terraform-deployment-react-redux-platform-image-bucket-s3.s3.amazonaws.com
USER root
RUN chown nginx:nginx /etc/nginx/nginx.conf
RUN chown nginx:nginx /etc/nginx/nginx.conf.default
RUN chmod +x /entrypoint.sh
USER nginx
CMD ["/entrypoint.sh"]