#!/bin/sh
set -e
export DOLLAR="$"
envsubst < /etc/nginx/nginx.conf.default > /etc/nginx/nginx.conf
nginx -g 'daemon off;'
